
import java.util.*;

public class PrimeThread extends Thread implements IPrime {
    private int x;
    private List<Integer> primes;
    public PrimeThread() {
	primes = new LinkedList<>();
    }
    public void run() {
	primes.add(2); 
	x = 3;
	while(true) {
	    ListIterator<Integer> it = primes.listIterator();
	    int i = 1;
	    boolean prime = true;
	    while (prime && it.hasNext() && i*i<=x) {
		i = it.next();
		prime &= (x%i != 0);
	    }
	    if (prime)
		primes.add(x);
	    x+=2;
	    try {
		Thread.sleep(100);
	    } catch (InterruptedException e) {
		System.out.println("Oops! " + e);
	    }
	}
    }
    public int getMax() {
	return x-1;
    }
    public boolean isPrime(int x) {
	return primes.contains(x);
    }
}

