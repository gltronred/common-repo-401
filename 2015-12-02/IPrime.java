
public interface IPrime {
    int getMax();
    boolean isPrime(int x);
}

