
import java.io.*;
import java.net.*;
import java.util.*;

public class ServerThread extends Thread {
    private IPrime primes;
    private Scanner sc;
    private PrintWriter pw;
    private int score;
    public ServerThread(IPrime primes, Socket s) throws IOException {
	this.primes = primes;
	sc = new Scanner(s.getInputStream());
	pw = new PrintWriter(s.getOutputStream());
	score = 0;
    }
    private void send(String message) {
	pw.println(message);
	pw.flush();
    }
    public void run() {
	Random random = new Random();
	while (true) {
	    int number;
	    do {
		number = 2 + random.nextInt(primes.getMax()-1);
	    } while (number % 2 == 0 || number % 3 == 0 || number % 5 == 0);
	    send("Is " + number + " prime?");
	    String ans;
	    do {
		ans = sc.nextLine();
	    } while (!ans.equals("y") && !ans.equals("n"));
	    boolean guess = ans.equals("y");
	    if (guess == primes.isPrime(number)) {
		send("You are right!");
		score++;
	    } else {
		send("You are wrong");
		score--;
	    }
	    send("Score: " + score);
	}
    }
}

