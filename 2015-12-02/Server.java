
import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    public static void main(String[] args) throws IOException {
	ServerSocket ss = new ServerSocket(3456);
	PrimeThread primeThread = new PrimeThread();
	primeThread.start();
	while(true) {
	    ServerThread serverThread = new ServerThread(primeThread, ss.accept());
	    serverThread.start();
	}
    }
}

